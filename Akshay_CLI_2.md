1) PS command :display a list of your processes that are currently running and obtain additional information about those processes

akshay@Akshay:~$ ps

    PID TTY          TIME CMD
   2920 pts/0    00:00:00 bash
   3255 pts/0    00:00:00 ps

2) List the top 3 processes by CPU usage.
   akshay@Akshay:~$ ps aux | sort -nrk 3,3 | head -n 3

3) List the top 3 processes by memory usage.
   akshay@Akshay:~$ ps -eo %mem --sort=-%mem | head -4

4) List the top
   akshay@Akshay:~$ top -h 

   procps-ng 3.3.17

  Usage:
  top -hv | -bcEeHiOSs1 -d secs -n max -u|U user -p pid(s) -o field -w [cols]

 Exit Top Command After Specific repetition: Top output keep refreshing until you press ‘q‘. With below command top command will automatically exit after 10 number of repetition.

 akshay@Akshay:~$ top -n 10

5) Ping
  * check ping virsion :
     akshay@Akshay:~$ sudo ping -v

  * find google Ip address : 
    akshay@Akshay:~$ ping www.google.com

    PING www.google.com (142.250.183.164) 56(84) bytes of data.
    64 bytes from bom07s32-in-f4.1e100.net (142.250.183.164): icmp_seq=1 ttl=58 time=26.9 ms

  * To Timeout PING: 
    To stop pinging after sometime use -w option. :
    akshay@Akshay:~$  ping -w 3 www.google.org

    PING www.google.org (216.239.32.27) 56(84) bytes of data.
    64 bytes from any-in-201b.1e100.net (216.239.32.27): icmp_seq=1 ttl=117 time=17.4 ms

  * how to check internet is working :
    akshay@Akshay:~$ ping -c 1 google.com

    PING google.com (142.250.192.142) 56(84) bytes of data.
    64 bytes from bom12s18-in-f14.1e100.net (142.250.192.142): icmp_seq=1 ttl=58 time=44.9 ms
    --- google.com ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 2008ms
    rtt min/avg/max/mdev = 19.230/28.376/44.947/11.738 ms


  * Time to wait for response: 

    akshay@Akshay:~$ ping -c 5 -W 3 www.google.org

    PING www.google.org (216.239.32.27) 56(84) bytes of data.

    64 bytes from any-in-201b.1e100.net (216.239.32.27): icmp_seq=1 ttl=117 time=40.0 ms

    64 bytes from any-in-201b.1e100.net (216.239.32.27): icmp_seq=2 ttl=117 time=18.8 ms

    64 bytes from any-in-201b.1e100.net (216.239.32.27): icmp_seq=3 ttl=117 time=18.7 ms

    64 bytes from any-in-201b.1e100.net (216.239.32.27): icmp_seq=4 ttl=117 time=16.1 ms

    64 bytes from any-in-201b.1e100.net (216.239.32.27): icmp_seq=5 ttl=117 time=18.0 ms
    --- www.google.org ping statistics ---

    5 packets transmitted, 5 received, 0% packet loss, time 4006ms

    rtt min/avg/max/mdev = 16.082/22.323/40.042/8.912 ms
6) ipconfig
   * Display a short list, instead of details :
     akshay@Akshay:~$ ifconfig -s
 
   * Run the command in verbose mode – log more details about execution :
     akshay@Akshay:~$ ifconfig -v
   
   * get help information :
     akshay@Akshay:~$ ifconfig --help


7) SSH
    ssh  command provides a secure encrypted connection between two hosts over an insecure network.
    ssh-keygen - creates a key pair for public key authentication

    * ssh-copy-id - configures a public key as authorized on a server

    * ssh-agent - agent to hold private key for single sign-on

    * ssh-add - tool to add a key to the agent

    * scp - file transfer client with   RCP-like command interface

    * sftp - file transfer client with FTP-like command interface

    * sshd - OpenSSH server

 8) lsof 

    * List all Open Files with lsof Command :
      akshay@Akshay:~$ lsof 

    * List User Specific Opened Files :
      akshay@Akshay:~$ lsof -u tecmint

    * Find Processes Running on Specific Port :
      akshay@Akshay:~$ lsof -i TCP:22  