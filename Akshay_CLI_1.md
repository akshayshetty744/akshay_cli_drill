1) to create folder "Hello" name

   akshay@Akshay:~/Desktop$ mkdir Hello


2) open Hello folder

   akshay@Akshay:~/Desktop$ cd Hello


3) inside Hello folder make directory "five" & "one"

   akshay@Akshay:~/Desktop/Home$ mkdir five

   akshay@Akshay:~/Desktop/Home$ mkdir one


4) inside "five" make directory "six"

   akshay@Akshay:~/Desktop/Home/$ cd five

   akshay@Akshay:~/Desktop/Home/five$ mkdir six

 
5) to create "c.text" file in six directory

   akshay@Akshay:~/Desktop/Home/five/$ cd six

   akshay@Akshay:~/Desktop/Home/five/six$ > c.text


6) make directory "seven" inside six    

   akshay@Akshay:~/Desktop/Home/five/six$ mkdir seven

   
7) make file "error.log" in seven directory

   akshay@Akshay:~/Desktop/Home/five/six$cd seven

   akshay@Akshay:~/Desktop/Home/five/six/seven$> error.log
 

8) Back in one directory inside make a.text & b.text file also make a directory "two"

   akshay@Akshay:~/Desktop/Home/five/six$cd ../../

   akshay@Akshay:~/Desktop/Home/$cd one

   akshay@Akshay:~/Desktop/Home/one$  

   akshay@Akshay:~/Desktop/Home/one$ > a.text

   akshay@Akshay:~/Desktop/Home/one$ > b.text

   akshay@Akshay:~/Desktop/Home/one$ mkdir two   

   
9) inside two make "d.text" file and directory "tree"

   akshay@Akshay:~/Desktop/Home/one$ cd two

   akshay@Akshay:~/Desktop/Home/one/two$> d.text

   akshay@Akshay:~/Desktop/Home/one/two$ mkdir tree

  
10) inside two make "e.text" file and directory "four" 

    akshay@Akshay:~/Desktop/Home/one/two$ cd tree

    akshay@Akshay:~/Desktop/Home/one/two/tree$ > e.text   

    akshay@Akshay:~/Desktop/Home/one/tree$ mkdir four


11) open directory four and make file "access.log"

    akshay@Akshay:~/Desktop/Home/one/two/tree/four$> access.log  


12) delete ".log" all files

    akshay@Akshay:~/Desktop/Home$ find . -type f -name "*.log" -exec rm -f {} \;


13) add content in a.text       

   akshay@Akshay:~/Desktop/Home$ cd one   

   akshay@Akshay:~/Desktop/Home/one$ echo "Unix is a family of multitasking,multiuser computer operating systems that derive from the original AT&T Unix,development starting in the 1970s at the Bell    Labs reseach center by Ken Thompson,Dennis Ritchie,and Others." >> a.txt
 

14) remove directory five

    akshay@Akshay:~/Desktop/Home/one$ cd ../

    akshay@Akshay:~/Desktop/Home$ rm -r five


15) ename the one directory to uno .

    akshay@Akshay:~/Desktop/Home$ mv one uno     


16) Move a.txt to the two directory.

    akshay@Akshay:~/Desktop/Home$ mv a.text two/
   
----------------------------------------------------------------------------------------------------
# Differences Between Relative Path and Absolute Path

 * Relative Path:
 Gives the file location in relation to the current working directory. Directories and sub-directories are separated by a slash '/', but do not begin with one.

 * Absolute Path:
 Gives an exact location of a file or directory name within a computer or file system. Begins with a slash '/' and directories and sub-directories are separated by a slash '/'. 

# Find out ctrl-c and ctrl-z
* Ctrl + C is used to send a SIGINT signal, which cancels or terminates the currently-running program. 
* Ctrl + Z is used to sends the TSTP signal, which pauses the program running in the foreground.

# Find out how to use Ctrl + R to reverse search
* use Ctrl+R as a keyboard shortcut to search through your command history

# Find out how to use the arrow keys to navigate history
* Down arrow key is used for browsing back the command list.